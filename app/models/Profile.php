<?php

class Profile extends \Eloquent {
	protected $fillable = [];

	public function user()
    {
        return $this->belongsTo('User');
    }

    public static function getByProvider($uid, $provider)
    {
    	$profile = Profile::where('uid', $uid)->where('provider', $provider)->first();

    	if(!empty($user)){
    		return $user;
    	} else{
    		return false;
    	}
    }
}