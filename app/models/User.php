<?php

class User extends \Eloquent {
	protected $fillable = [];

	public function profiles()
    {
        return $this->hasMany('Profile');
    }
}