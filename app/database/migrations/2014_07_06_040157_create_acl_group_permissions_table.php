<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAclGroupPermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('acl_group_permissions', function ($table) {
 			
 			$table->increments('id');
            $table->integer('group_id')->unsigned()->index();
            $table->integer('permission_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
 
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('acl_group_permissions');
	}

}
