<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::filter('acl.permitted', 'AclPermittedFilter');
 
// Route::group(array('prefix' => 'user'), function() {
  
//     // other routes here
//     Route::get('supersecret', array(
//         'before'    => ['auth', 'acl.permitted'],
//         'as'        => 'user.supersecret',
//         'uses'      => 'UserController@supersecret'
//     ))
    
// });

Route::get('/', 'HomeController@index');

Route::get('oauth/login/{action?}','OAuthController@login');
Route::get('oauth/logout', 'OAuthController@logout');


