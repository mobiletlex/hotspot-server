<?php
	// use Illuminate\Filesystem\Filesystem;
	/**
	* Class FileHelper
	*/
	class FileHelper
	{

		const TMP_PATH = '/uploads/tmp/';

		protected static $mime_types  = array(
            // images
            'png' => 'image/png',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml'
        );

        public static function getExtenstionFromMimeType($mime_type)
        {
        	
        	foreach (self::$mime_types as $key => $value) {
        		if($value == $mime_type){
        			return $key;
        		}
        	}

        	return '';

        }
		
		public static function saveFileFromUrl($url, $target, $filename = false)
		{
			$tmpUniquePath = public_path() . self::TMP_PATH . uniqid();

			if(file_put_contents($tmpUniquePath, file_get_contents($url))){

				$extension = self::getExtenstionFromMimeType(mime_content_type($tmpUniquePath));

				$targetFile = $target . $filename . "." . $extension;

				if(File::move($tmpUniquePath, $targetFile)){

					return $targetFile;

				} else{

					return false;

				}

			} else{
				return false;
			}
		}

		public static function fileForcePutContents($dir, $contents){

	        $parts = explode('/', $dir);
	        
	        $file = array_pop($parts);
	        
	        $dir = '';
	        foreach($parts as $part){

	            if(!is_dir($dir .= "/$part")){

	            	mkdir($dir);

	            };

	        }
	        file_put_contents("$dir/$file", $contents);
	    }
		
	}