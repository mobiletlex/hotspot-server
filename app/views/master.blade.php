<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{ $title }}</title>
	
	{{ HTML:style('css/bootstrap.css') }}
	{{ HTML:style('css/common.css') }}

	{{ HTML:script('js/jquery.js') }}
	{{ HTML:script('js/bootstrap.js') }}
</head>
<body>
	<!-- Header -->
	@include('header')

	<!-- Content -->
	<div id="content" class="container">
		<div class="row">
			<!-- Left Content -->
			<div class="col-sm-3">
				@include('left_content')
			</div>

			<!-- Main Content -->
			<div class="col-sm-6">
				@yield('content')
			</div>

			<!-- Right Content -->
			<div class="col-sm-3">
				@include('right_content')
			</div>
		</div>
	</div>

	<!-- Footer -->
	@include('footer')
	
</body>
</html>