@extends('layouts.master')

@section('content')
    <div class="well">
    	<a id="fblogin_btn" href="/oauth/login?provider=facebook" class="btn btn-info">Login with Facebook</a>
    	<a id="twitterlogin_btn" href="/oauth/login?provider=twitter" class="btn btn-info">Login with Twitter</a>
    </div>	
@stop