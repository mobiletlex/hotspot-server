<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Title</title>
	
	{{ HTML::style('css/bootstrap.css') }}

	{{ HTML::script('js/jquery.js') }}
	{{ HTML::script('js/bootstrap.js') }}

</head>
<body>
	<!-- Header -->
	@include('includes.header')

	<!-- Content -->
	<div id="content" class="container">
		<div class="row">
			<!-- Left Content -->
			<div class="col-sm-3">
				@include('includes.left_content')
			</div>

			<!-- Main Content -->
			<div class="col-sm-6">
				@yield('content')
			</div>

			<!-- Right Content -->
			<div class="col-sm-3">
				@include('includes.right_content')
			</div>
		</div>
	</div>

	<!-- Footer -->
	@include('includes.footer')
	
</body>
</html>