<?php

class AclPermittedFilter {
 
    public function filter($route, $request)
    {
        $permitted = false;
        
        $user = Auth::user();
        $user->load('groups', 'group.permissions');
        
        foreach($user->groups as $group) {
            if($group->permissions->has($route->getName())) {
                $permitted = true;
                break;
            }
        }
        
        if(!$permitted) {
            return Redirect::route('user.denied');
        }
    }
 
}