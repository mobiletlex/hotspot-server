<?php

class OAuthController extends \BaseController {

	public function login($action = '')
    {
   		$provider = Input::get('provider');

		if ( $action == "auth" ) {
			try {
			 Hybrid_Endpoint::process();
			}
			catch ( Exception $e ) {
			 echo "Error at Hybrid_Endpoint process (OAuthController@login): $e";
			}
			return;
   		}

 		// initialize Hybrid_Auth with a given file
		$hybridauth = new Hybrid_Auth( Config::get('hybridauth'));
 
		// try to authenticate with the selected provider
		$adapter = $hybridauth->authenticate($provider);
 
		// then grab the user profile 
		$userProfile = $adapter->getUserProfile();

		$profile = Profile::getByProvider($userProfile->identifier, $provider);

		if(!$profile){

			$destinationPath = public_path() . '/uploads/images/users/';
			$savedPhoto = FileHelper::saveFileFromUrl($userProfile->photoURL, $destinationPath, 'avatar');
			
			echo '<img src="' . link_to_asset($savedPhoto) . '" />';
		}
    }
 
    public function logout()
    {
 		$this->hybridAuth->logoutAllProviders();
    }	
}