$(document).ready(function(){
    Facebook.setup();
});

var Facebook = {
    loginURL: '/users/login_fb',

    login: function(authResponse){
        var data = {
            uid: authResponse.userID,
            access_token: authResponse.accessToken,
            provider: 'facebook'
        };

        $.ajax({
            url: this.loginURL,
            type: 'POST',
            data: data,
            success: function(response){
                console.log(response);
            }
        });    
    },

    setupLoginBtn: function(){
        $("#fblogin_btn").click(function(){
            FB.login(function(response) {
                if (response.status === 'connected') {
                    // Logged into your app and Facebook.
                    Facebook.login(response.authResponse);
                } else if (response.status === 'not_authorized') {
                    // The person is logged into Facebook, but not your app.
                } else {
                    // The person is not logged into Facebook, so we're not sure if
                    // they are logged into this app or not.
                }
            }, {scope: 'public_profile,email'});
        });
    },

    setup: function(){
        this.setupLoginBtn();
    }
};

